const path = require("path");
const NodemonPlugin = require("nodemon-webpack-plugin");
var nodeExternals = require("webpack-node-externals");
//const CopyPlugin = require("copy-webpack-plugin");

const config = {
  context: path.resolve(__dirname, "src"),
  entry: {
    main: "./index.js",
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "dist"),
    library: "[name]",
    libraryTarget: "umd",
  },
  target: "node",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader", // babel-runtime НЕ ЗАБЫТЬ ДОБАВИТЬ!!!
        },
      },
    ],
  },
  externals: [nodeExternals()],
  resolve: {
    modules: ["node_modules"],
  },
  plugins: [
    new NodemonPlugin(),
    /*
    new CopyPlugin({
      patterns: [
        {
          from: path.posix.join(
            path.resolve(__dirname, "contracts").replace(/\\/g, "/"),
            "*.proto"
          ),
          to: "contracts",
          noErrorOnMissing: true
        },
      ],
    }),
    */
  ],
  resolve: {
    extensions: ["*", ".js"],
    alias: {
      src: path.resolve(__dirname, "src"),
      root: path.resolve(__dirname),
      server: path.resolve(__dirname, "src/server"),
    },
  }
};

module.exports = [ config ];
