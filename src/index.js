var odata = require('node-odata');

var server = odata('mongodb://localhost/test');

server.resource('books', {
  title: String,
  price: Number
});

server.listen(3000);